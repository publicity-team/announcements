#use wml::debian::translation-check translation="xxxx" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.6</define-tag>
<define-tag release_date>2020-09-26</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.6</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la sixième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version
de Debian <release> mais seulement une mise à jour de certains des paquets
qu'elle contient. Il n'est pas nécessaire de jeter les anciens médias de
la version <codename>. Après installation, les paquets peuvent être mis à
niveau vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette
mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP
de Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections
importantes aux paquets suivants :
</p>

<p>Veuillez noter que, du fait de problèmes de construction, les mises à
jour des paquets cargo, rustc et rustc-bindgen ne sont actuellement pas
disponibles pour l'architecture <q>armel</q>. Elles seront ajoutées
ultérieurement si les problèmes sont résolus.</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction arch-test "Correction de la détection de s390x qui échoue parfois">
<correction asterisk "Correction de plantage lors d'une négociation pour T.38 avec un flux refusé [CVE-2019-15297], de <q>une requête SIP peut modifier l'adresse d'un pair SIP</q> [CVE-2019-18790], de <q>l'utilisateur AMI pourrait exécuter des commandes système</q> [CVE-2019-18610], d'une erreur de segmentation dans la commande <q>pjsip show history</q> avec les pairs IPv6">
<correction bacula "Correction de <q>des chaînes d’empreinte surdimensionnées permettent à un client malveillant de provoquer un dépassement de tas dans la mémoire du directeur</q> [CVE-2020-11061]">
<correction base-files "Mise à jour de /etc/debian_version pour cette version">
<correction calamares-settings-debian "Désactivation du module displaymanager">
<correction cargo "Nouvelle version amont pour prendre en charge les versions à venir de Firefox ESR">
<correction chocolate-doom "Correction d'absence de validation [CVE-2020-14983]">
<correction chrony "Compétition de lien symbolique évitée lors de l'écriture dans le fichier PID [CVE-2020-14367] ; correction de la lecture de température">
<correction debian-installer "Mise à jour de l'ABI Linux vers la version 4.19.0-11">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction diaspora-installer "Utilisation de l'option --frozen pour grouper l'installation afin d'utiliser le Gemfile.lock de l'amont ; plus d'exclusion de Gemfile.lock lors des mises à niveau ; plus d'écrasement de config/oidc_key.pem lors des mises à niveau ; config/schedule.yml accessible en écriture">
<correction dojo "Correction d'une pollution de prototype dans la méthode deepCopy [CVE-2020-5258] et la méthode jqMix [CVE-2020-5259]">
<correction dovecot "Correction de la régression de la synchronisation du filtre Sieve de dsync ; correction de la gestion du résultat de getpwent dans userdb-passwd">
<correction facter "Changement du point de terminaison de métadonnées de Google GCE de <q>v1beta1</q> à <q>v1</q>">
<correction gnome-maps "Correction d'un problème de décalage dans le rendu des calques de forme">
<correction gnome-shell "LoginDialog : remise à zéro de l'affichage du mot de passe au moment de du changement de terminal virtuel et sa disparition [CVE-2020-17489]">
<correction gnome-weather "Plantage évité quand la configuration des localisations n'est pas valable">
<correction grunt "Utilisation de safeLoad lors du chargement de fichiers YAML [CVE-2020-7729]">
<correction gssdp "Nouvelle version amont stable">
<correction gupnp "Nouvelle version amont stable ; attaque <q>CallStranger</q> évitée [CVE-2020-12695] ; GSSDP 1.0.5 requis">
<correction haproxy "logrotate.conf : utilisation de l'assistant rsyslog à la place d'un script de démarrage SysV ; rejet des messages où <q>chunked</q> est absent dans Transfer-Encoding [CVE-2019-18277]">
<correction icinga2 "Correction d'attaque par lien symbolique [CVE-2020-14004]">
<correction incron "Correction de nettoyage de processus zombies">
<correction inetutils "Correction d'un problème d'exécution de code à distance [CVE-2020-10188]">
<correction libcommons-compress-java "Correction d'un problème de déni de [CVE-2019-12402]">
<correction libdbi-perl "Correction de corruption de mémoire dans les fonctions XS quand la pile de Perl est réallouée [CVE-2020-14392] ; correction d'un dépassement de tampon sur un nom de classe DBD trop long [CVE-2020-14393] ; correction d'un déréférencement de profil NULL dans dbi_profile() [CVE-2019-20919]">
<correction libvncserver "libvncclient : abandon si le nom du socket UNIX peut déborder [CVE-2019-20839] ; correction d'un problème de chevauchement ou d'alignement de pointeur [CVE-2020-14399] ; taille maximale de textchat limitée [CVE-2020-14405] ; libvncserver : ajout de vérifications manquantes de pointeur NULL [CVE-2020-14397] ; correction d'un problème de chevauchement ou d'alignement de pointeur [CVE-2020-14400] ; scale : forçage en 64 bits avant le déplacement [CVE-2020-14401] ; accès à hors limites évités [CVE-2020-14402 CVE-2020-14403 CVE-2020-14404]">
<correction libx11 "Correction de dépassements d'entier [CVE-2020-14344 CVE-2020-14363]">
<correction lighttpd "Rétroportage de plusieurs corrections d'accessibilité et de sécurité">
<correction linux "Nouvelle version amont stable ; passage de l'ABI à la version 11">
<correction linux-latest "Mise à jour de l'ABI du noyau linux à la version -11">
<correction linux-signed-amd64 "Nouvelle version amont stable">
<correction linux-signed-arm64 "Nouvelle version amont stable">
<correction linux-signed-i386 "Nouvelle version amont stable">
<correction llvm-toolchain-7 "Nouvelle version amont pour prendre en charge les versions à venir de Firefox ESR ; correction de bogues affectant la construction de rustc">
<correction lucene-solr "Correction d'un problème de sécurité dans la gestion de la configuration de DataImportHandler [CVE-2019-0193]">
<correction milkytracker "Correction d'un dépassement de tas [CVE-2019-14464], d'un dépassement de pile [CVE-2019-14496], d'un dépassement de tas [CVE-2019-14497], d'une utilisation de mémoire après libération [CVE-2020-15569]">
<correction node-bl "Correction d'une vulnérabilité de lecture excessive [CVE-2020-8244]">
<correction node-elliptic "Malléabilité et dépassements évités [CVE-2020-13822]">
<correction node-mysql "Ajout de l'option localInfile pour contrôler LOAD DATA LOCAL INFILE [CVE-2019-14939]">
<correction node-url-parse "Correction de validation et de nettoyage insuffisants des entrées de l'utilisateur [CVE-2020-8124]">
<correction npm "Pas d'affichage de mot de passe dans les journaux [CVE-2020-15095]">
<correction orocos-kdl "Retrait de l'inclusion explicite du chemin inclus par défaut, corrigeant des problèmes avec cmake &lt; 3.16">
<correction postgresql-11 "Nouvelle version amont stable ; définition d'un chemin de recherche sûr dans les processus walsender de réplication logique et les <q>workers</p> d'application [CVE-2020-14349] ; scripts d'installation des modules contrib plus sûrs [CVE-2020-14350]">
<correction postgresql-common "Pas de retrait de plpgsql avant de tester les extensions">
<correction pyzmq "Asyncio : attente de POLLOUT sur l'expéditeur dans can_connect">
<correction qt4-x11 "Correction de dépassement de tampon dans l'analyseur XBM [CVE-2020-17507]">
<correction qtbase-opensource-src "Correction de dépassement de tampon dans l'analyseur XBM [CVE-2020-17507] ; correction de la fin du presse-papier quand le temporisateur se réinitialise après 50 jours">
<correction ros-actionlib "Chargement sûr de YAML [CVE-2020-10289]">
<correction rustc "Nouvelle version amont pour prendre en charge les versions à venir de Firefox ESR">
<correction rust-cbindgen "Nouvelle version amont pour prendre en charge les versions à venir de Firefox ESR">
<correction ruby-ronn "Correction de la gestion du contenu en UTF-8 dans les pages de manuel">
<correction s390-tools "Dépendance de Perl codée en dur à la place de l'utilisation de ${perl:Depends}, corrigeant l'installation avec debootstrap">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2020 4662 openjdk-11>
<dsa 2020 4734 openjdk-11>
<dsa 2020 4736 firefox-esr>
<dsa 2020 4737 xrdp>
<dsa 2020 4738 ark>
<dsa 2020 4739 webkit2gtk>
<dsa 2020 4740 thunderbird>
<dsa 2020 4741 json-c>
<dsa 2020 4742 firejail>
<dsa 2020 4743 ruby-kramdown>
<dsa 2020 4744 roundcube>
<dsa 2020 4745 dovecot>
<dsa 2020 4746 net-snmp>
<dsa 2020 4747 icingaweb2>
<dsa 2020 4748 ghostscript>
<dsa 2020 4749 firefox-esr>
<dsa 2020 4750 nginx>
<dsa 2020 4751 squid>
<dsa 2020 4752 bind9>
<dsa 2020 4753 mupdf>
<dsa 2020 4754 thunderbird>
<dsa 2020 4755 openexr>
<dsa 2020 4756 lilypond>
<dsa 2020 4757 apache2>
<dsa 2020 4758 xorg-server>
<dsa 2020 4759 ark>
<dsa 2020 4760 qemu>
<dsa 2020 4761 zeromq3>
<dsa 2020 4762 lemonldap-ng>
<dsa 2020 4763 teeworlds>
<dsa 2020 4764 inspircd>
<dsa 2020 4765 modsecurity>
</table>



<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés
dans cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres
qui offrent volontairement leur temps et leurs efforts pour produire le
système d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de
publication de la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>

#use wml::debian::translation-check translation="xxxx" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 10.9</define-tag>
<define-tag release_date>2021-03-27</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>10</define-tag>
<define-tag codename>Buster</define-tag>
<define-tag revision>10.9</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la neuvième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans ce
document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>


<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction avahi "Retrait du mécanisme avahi-daemon-check-dns qui n'est plus nécessaire">
<correction base-files "Mise à jour de /etc/debian_version pour cette version">
<correction cloud-init "Plus d'enregistrement des mots de passe générés dans des fichiers journaux lisibles par tous [CVE-2021-3429]">
<correction debian-archive-keyring "Ajout des clés de Bullseye ; retrait des clés de Jessie">
<correction debian-installer "Utilisation de l'ABI du noyau Linux 4.19.0-16">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction exim4 "Correction de l'utilisation de connexions TLS concurrentes sous GnuTLS ; correction de la vérification des certificats TLS avec les CNAME ; README.Debian : documentation de la portée et des limites de la vérification de certificat du serveur dans la configuration par défaut">
<correction fetchmail "Plus de rapport <q>System error during SSL_connect(): Success</q> ; retrait de la vérification de la version d'OpenSSL">
<correction fwupd "Ajout de la prise en charge de SBAT">
<correction fwupd-amd64-signed "Ajout de la prise en charge de SBAT">
<correction fwupd-arm64-signed "Ajout de la prise en charge de SBAT">
<correction fwupd-armhf-signed "Ajout de la prise en charge de SBAT">
<correction fwupd-i386-signed "Ajout de la prise en charge de SBAT">
<correction fwupdate "Ajout de la prise en charge de SBAT">
<correction fwupdate-amd64-signed "Ajout de la prise en charge de SBAT">
<correction fwupdate-arm64-signed "Ajout de la prise en charge de SBAT">
<correction fwupdate-armhf-signed "Ajout de la prise en charge de SBAT">
<correction fwupdate-i386-signed "Ajout de la prise en charge de SBAT">
<correction gdnsd "Correction de dépassement de pile avec des adresses IPV6 excessivement longues [CVE-2019-13952]">
<correction groff "Reconstruction avec ghostscript 9.27">
<correction hwloc-contrib "Activation de la prise en charge de l'architecture ppc64el">
<correction intel-microcode "Mise à jour de divers microprogrammes">
<correction iputils "Correction d'erreurs d'arrondi de ping ; correction de corruption de l'adresse des cibles de tracepath">
<correction jquery "Correction de vulnérabilités d'exécution de code non fiable [CVE-2020-11022 CVE-2020-11023]">
<correction libbsd "Correction d'un problème de lecture hors limites [CVE-2019-20367]">
<correction libpano13 "Correction d'une vulnérabilité de format de chaîne">
<correction libreoffice "Pas de chargement d'encodings.py à partir du répertoire courant">
<correction linux "Nouvelle version amont stable ; mise à jour de l'ABI pour la version -16 ; rotation des clés de signature de Secure Boot ; rt : mise à jour pour la version 4.19.173-rt72">
<correction linux-latest "Mise à jour pour l'ABI du noyau -15  ; mise à jour pour l'ABI du noyau -16">
<correction linux-signed-amd64 "Nouvelle version amont stable ; mise à jour pour l'ABI -16 ; rotation des clés de signature de Secure Boot ; rt : mise à jour pour la version 4.19.173-rt72">
<correction linux-signed-arm64 "Nouvelle version amont stable ; mise à jour pour l'ABI -16 ; rotation des clés de signature de Secure Boot ; rt : mise à jour pour la version 4.19.173-rt72">
<correction linux-signed-i386 "Nouvelle version amont stable ; mise à jour pour l'ABI -16 ; rotation des clés de signature de Secure Boot ; rt : mise à jour pour la version 4.19.173-rt72">
<correction lirc "Normalisation de la valeur incorporée dans ${DEB_HOST_MULTIARCH} dans /etc/lirc/lirc_options.conf pour trouver les fichiers de configuration non modifiés dans toutes les architectures ; gir1.2-vte-2.91 recommandé à la place de gir1.2-vte qui n'existe pas">
<correction m2crypto "Correction d'échec de tests avec les versions récentes d'OpenSSL">
<correction openafs "Correction des connexions sortantes après le temps de l'heure Unix 0x60000000 (14 janvier 2021)">
<correction portaudio19 "Gestion d'EPIPE à partir de alsa_snd_pcm_poll_descriptors, corrigeant un plantage">
<correction postgresql-11 "Nouvelle version amont stable ; correction de fuite d'information dans les messages d'erreur de violation de contrainte [CVE-2021-3393] ; correction de CREATE INDEX CONCURRENTLY pour attendre les transactions préparées concurrentes">
<correction privoxy "Problèmes de sécurité [CVE-2020-35502 CVE-2021-20209 CVE-2021-20210 CVE-2021-20211 CVE-2021-20212 CVE-2021-20213 CVE-2021-20214 CVE-2021-20215 CVE-2021-20216 CVE-2021-20217 CVE-2021-20272 CVE-2021-20273 CVE-2021-20275 CVE-2021-20276]">
<correction python3.7 "Correction d'injection de fin de ligne (CRLF) dans http.client [CVE-2020-26116] ; correction de dépassement de tampon dans PyCArg_repr dans _ctypes/callproc.c [CVE-2021-3177]">
<correction redis "Correction d'une série de problèmes de dépassement d'entier sur les systèmes 32 bits [CVE-2021-21309]">
<correction ruby-mechanize "Correction d'un problème d'injection de commande [CVE-2021-21289]">
<correction systemd "core : restauration de l'id de la commande de contrôle assurée, aussi, corrigeant une erreur de segmentation ; seccomp : désactivation du filtrage de seccomp permise au moyen d'une variable d'environnement">
<correction uim "libuim-data : réalisation de la conversion symlink_to_dir de /usr/share/doc/libuim-data dans le paquet recréé pour des mises à niveau propres à partir de Stretch">
<correction xcftools "Correction d'une vulnérabilité de dépassement d'entier [CVE-2019-5086 CVE-2019-5087]">
<correction xterm "Correction de la limite supérieure du tampon de sélection, tenant compte des caractères combinés [CVE-2021-27135]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2021 4826 nodejs>
<dsa 2021 4844 dnsmasq>
<dsa 2021 4845 openldap>
<dsa 2021 4846 chromium>
<dsa 2021 4847 connman>
<dsa 2021 4849 firejail>
<dsa 2021 4850 libzstd>
<dsa 2021 4851 subversion>
<dsa 2021 4853 spip>
<dsa 2021 4854 webkit2gtk>
<dsa 2021 4855 openssl>
<dsa 2021 4856 php7.3>
<dsa 2021 4857 bind9>
<dsa 2021 4858 chromium>
<dsa 2021 4859 libzstd>
<dsa 2021 4860 openldap>
<dsa 2021 4861 screen>
<dsa 2021 4862 firefox-esr>
<dsa 2021 4863 nodejs>
<dsa 2021 4864 python-aiohttp>
<dsa 2021 4865 docker.io>
<dsa 2021 4867 grub-efi-amd64-signed>
<dsa 2021 4867 grub-efi-arm64-signed>
<dsa 2021 4867 grub-efi-ia32-signed>
<dsa 2021 4867 grub2>
<dsa 2021 4868 flatpak>
<dsa 2021 4869 tiff>
<dsa 2021 4870 pygments>
<dsa 2021 4871 tor>
<dsa 2021 4872 shibboleth-sp>
</table>



<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/stable/">
</div>

<p>
Mises à jour proposées à la distribution stable :
</p>

<div class="center">
  <url "http://ftp.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contact Information</h2>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>

#use wml::debian::translation-check translation="xxxxxxxxxxxxxxxx" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 12.7</define-tag>
<define-tag release_date>2024-08-31</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>12</define-tag>
<define-tag codename>Bookworm</define-tag>
<define-tag revision>12.7</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la septième mise à jour de sa
distribution stable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version stable. Les annonces de
sécurité ont déjà été publiées séparément et sont simplement référencées dans
ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions actuelles en utilisant un miroir Debian à jour.
</p>

<p>
Les personnes qui installent fréquemment les mises à jour à partir de
security.debian.org n'auront pas beaucoup de paquets à mettre à jour et la
plupart des mises à jour de security.debian.org sont comprises dans cette mise
à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>

<h2>Secure Boot et les autres systèmes d'exploitation</h2>

<p>
Les utilisateurs qui démarrent d'autres systèmes d'exploitation sur la
même machine et où le démarrage sécurisé (Secure Boot) est activé, doivent
savoir que shim 15.8 (inclus dans Debian <revision>) révoque les signatures
de toutes les versions antérieures de shim dans le microprogramme UEFI.
Cela peut empêcher de démarrer tous les autres systèmes d'exploitation
utilisant des versions de shim antérieures à 15.8.
</p>

<p>
Les utilisateurs affectés peuvent désactiver temporairement le démarrage
sécurisé avant de mettre à jour les autres systèmes d’exploitation.
</p>


<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version stable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction amd64-microcode "Nouvelle version stable ; corrections de sécurité [CVE-2023-31315] ; corrections du microprogramme SEV [CVE-2023-20584 CVE-2023-31356]">
<correction ansible "Nouvelle version amont stable ; correction d'un problème de fuite de clé [CVE-2023-4237]">
<correction ansible-core "Nouvelle version amont stable ; correction d'un problème de divulgation d'informations [CVE-2024-0690] ; correction d'un problème d'injection de modèle [CVE-2023-5764] ; correction d'un problème de traversée de répertoire [CVE-2023-5115]">
<correction apache2 "Nouvelle version amont stable ; correction d'un problème de divulgation de contenu [CVE-2024-40725]">
<correction base-files "Mise à jour pour cette version">
<correction cacti "Correction de problèmes d'exécution de code à distance [CVE-2024-25641 CVE-2024-31459], de problèmes de script intersite [CVE-2024-29894 CVE-2024-31443 CVE-2024-31444], de problèmes d'injection de code SQL [CVE-2024-31445 CVE-2024-31458 CVE-2024-31460], d'un problème de <q>jonglage de type</q> [CVE-2024-34340] ; correction d'échec d'autopkgtest">
<correction calamares-settings-debian "Correction d'un problème de permission du lanceur de Xfce">
<correction calibre "Correction d'un problème d'exécution de code [CVE-2024-6782], d'un problème de script intersite [CVE-2024-7008], d'un problème d'injection de code SQL [CVE-2024-7009]">
<correction choose-mirror "Mise à jour de la liste des miroirs disponibles">
<correction cockpit "Correction d'un problème de déni de service [CVE-2024-6126]">
<correction cups "Correction de problèmes de gestion de socket de domaine [CVE-2024-35235]">
<correction curl "Correction d'un problème de lecture hors limite lors de l'analyse de date d'ASN.1 [CVE-2024-7264]">
<correction cyrus-imapd "Correction d'une régression introduite dans la correction de CVE-2024-34055">
<correction dcm2niix "Correction d'un potentiel problème d'exécution de code [CVE-2024-27629]">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 6.1.0-25 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction dmitry "Corrections de sécurité [CVE-2024-31837 CVE-2020-14931 CVE-2017-7938]">
<correction dropbear "Correction du comportement <q>noremotetcp</q> des paquets keepalive en combinaison avec la restriction <q>no-port-forwarding</q> d'authorized_keys(5)">
<correction gettext.js "Correction d'un problème de contrefaçon de requête côté serveur [CVE-2024-43370]">
<correction glibc "Correction de libération de mémoire non initialisée dans libc_freeres_fn() ; correction de plusieurs problèmes de performance et de plantages potentiels">
<correction glogic "Gtk 3.0 et PangoCairo 1.0 requis">
<correction graphviz "Correction d'une échelle cassée">
<correction gtk+2.0 "Recherche évitée de modules dans le répertoire de travail en cours [CVE-2024-6655]">
<correction gtk+3.0 "Recherche évitée de modules dans le répertoire de travail en cours [CVE-2024-6655]">
<correction imagemagick "Correction d'un problème d'erreur de segmentation ; correction du correctif incomplet pour le CVE-2023-34151">
<correction initramfs-tools "hook_functions : correction de copy_file avec une source incluant un lien symbolique de répertoire ; hook-functions : copy_file : normalisation du nom de fichier de la cible ; installation du module hid-multitouch pour le clavier Surface Pro 4 ; ajout du module hyper-keyboard requis pour la saisie du mot de passe LUKS dans Hyper-V ; auto_add_modules : ajout de onboard_usb_hub et de onboard_usb_dev">
<correction intel-microcode "Nouvelle version stable ; corrections de sécurité [CVE-2023-42667 CVE-2023-49141 CVE-2024-24853 CVE-2024-24980 CVE-2024-25939]">
<correction ipmitool "Ajout du fichier manquant enterprise-numbers.txt">
<correction libapache2-mod-auth-openidc "Plantage évité quand l'en-tête Forwarded est absent mais que OIDCXForwardedHeaders est configuré pour cet en-tête">
<correction libnvme "Correction d'un dépassement de tampon durant l'analyse des périphériques qui ne prennent pas en charge les lectures de blocs inférieurs à 4 ko">
<correction libvirt "birsh : possibilité que domif-setlink fonctionne plus d'une fois ; qemu : domain : correction de la logique lors de l'altération du domaine ; correction de problèmes de déni de service [CVE-2023-3750 CVE-2024-1441 CVE-2024-2494 CVE-2024-2496]">
<correction linux "Nouvelle version stable ; passage de l'ABI à la version 25">
<correction linux-signed-amd64 "Nouvelle version stable ; passage de l'ABI à la version 25">
<correction linux-signed-arm64 "Nouvelle version stable ; passage de l'ABI à la version 25">
<correction linux-signed-i386 "Nouvelle version stable ; passage de l'ABI à la version 25">
<correction newlib "Correction d'un problème de dépassement de tampon [CVE-2021-3420]">
<correction numpy "Conflit avec python-numpy">
<correction openssl "Nouvelle version amont stable ; correction de problèmes de déni de service [CVE-2024-2511 CVE-2024-4603] ; correction d'un problème d'utilisation de mémoire après libération [CVE-2024-4741]">
<correction poe.app "Cellules de commentaire éditables ; correction du dessin quand une <q>NSActionCell</q> dans les préférences applique un changement d'état">
<correction putty "Correction d'une génération de nombre arbitraire ECDSA faible permettant la récupération de la clé secrète [CVE-2024-31497]">
<correction qemu "Nouvelle version amont stable ; correction d'un problème de déni de service [CVE-2024-4467]">
<correction riemann-c-client "Charge utile malformée évitée dans les opérations envoi/réception de GnuTLS">
<correction rustc-web "Nouvelle version amont stable pour prendre en charge la construction des nouvelles versions de Chromium et de Firefox ESR">
<correction shim "Nouvelle version stable">
<correction shim-helpers-amd64-signed "Reconstruction avec shim 15.8.1">
<correction shim-helpers-arm64-signed "Reconstruction avec shim 15.8.1">
<correction shim-helpers-i386-signed "Reconstruction avec shim 15.8.1">
<correction shim-signed "Nouvelle version amont stable">
<correction systemd "Nouvelle version amont stable ; mise à jour de hwdb">
<correction usb.ids "Mise à jour de la liste des données incluses">
<correction xmedcon "Correction d'un problème de dépassement de tampon [CVE-2024-29421]">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
stable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2024 5617 chromium>
<dsa 2024 5629 chromium>
<dsa 2024 5634 chromium>
<dsa 2024 5636 chromium>
<dsa 2024 5639 chromium>
<dsa 2024 5648 chromium>
<dsa 2024 5654 chromium>
<dsa 2024 5656 chromium>
<dsa 2024 5668 chromium>
<dsa 2024 5675 chromium>
<dsa 2024 5676 chromium>
<dsa 2024 5683 chromium>
<dsa 2024 5687 chromium>
<dsa 2024 5689 chromium>
<dsa 2024 5694 chromium>
<dsa 2024 5696 chromium>
<dsa 2024 5697 chromium>
<dsa 2024 5701 chromium>
<dsa 2024 5710 chromium>
<dsa 2024 5716 chromium>
<dsa 2024 5719 emacs>
<dsa 2024 5720 chromium>
<dsa 2024 5722 libvpx>
<dsa 2024 5723 plasma-workspace>
<dsa 2024 5724 openssh>
<dsa 2024 5725 znc>
<dsa 2024 5726 krb5>
<dsa 2024 5727 firefox-esr>
<dsa 2024 5728 exim4>
<dsa 2024 5729 apache2>
<dsa 2024 5731 linux-signed-amd64>
<dsa 2024 5731 linux-signed-arm64>
<dsa 2024 5731 linux-signed-i386>
<dsa 2024 5731 linux>
<dsa 2024 5732 chromium>
<dsa 2024 5734 bind9>
<dsa 2024 5735 chromium>
<dsa 2024 5737 libreoffice>
<dsa 2024 5738 openjdk-17>
<dsa 2024 5739 wpa>
<dsa 2024 5740 firefox-esr>
<dsa 2024 5741 chromium>
<dsa 2024 5743 roundcube>
<dsa 2024 5745 postgresql-15>
<dsa 2024 5748 ffmpeg>
<dsa 2024 5749 bubblewrap>
<dsa 2024 5749 flatpak>
<dsa 2024 5750 python-asyncssh>
<dsa 2024 5751 squid>
<dsa 2024 5752 dovecot>
<dsa 2024 5753 aom>
<dsa 2024 5754 cinder>
<dsa 2024 5755 glance>
<dsa 2024 5756 nova>
<dsa 2024 5757 chromium>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction bcachefs-tools "Bogué ; obsolète">

</table>



<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de stable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution stable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/stable/">
</div>

<p>Mises à jour proposées à la distribution stable :</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/proposed-updates">
</div>

<p>
Informations sur la distribution stable (notes de publication,
<i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/stable/">https://www.debian.org/releases/stable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.
</p>



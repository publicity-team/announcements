#use wml::debian::translation-check translation="XXXXXXXXX" maintainer="Jean-Pierre Giraud"
<define-tag pagetitle>Publication de la mise à jour de Debian 11.08</define-tag>
<define-tag release_date>2023-10-07</define-tag>
#use wml::debian::news
# $Id:

<define-tag release>11</define-tag>
<define-tag codename>Bullseye</define-tag>
<define-tag revision>11.8</define-tag>

<define-tag dsa>
    <tr><td align="center"><a href="$(HOME)/security/%0/dsa-%1">DSA-%1</a></td>
        <td align="center"><:
    my @p = ();
    for my $p (split (/,\s*/, "%2")) {
	push (@p, sprintf ('<a href="https://packages.debian.org/src:%s">%s</a>', $p, $p));
    }
    print join (", ", @p);
:></td></tr>
</define-tag>

<define-tag correction>
    <tr><td><a href="https://packages.debian.org/src:%0">%0</a></td>              <td>%1</td></tr>
</define-tag>

<define-tag srcpkg><a href="https://packages.debian.org/src:%0">%0</a></define-tag>

<p>
Le projet Debian a l'honneur d'annoncer la huitième mise à jour de sa
distribution oldstable Debian <release> (nom de code <q><codename></q>).
Tout en réglant quelques problèmes importants, cette mise à jour corrige
principalement des problèmes de sécurité de la version oldstable. Les annonces
de sécurité ont déjà été publiées séparément et sont simplement référencées
dans ce document.
</p>

<p>
Veuillez noter que cette mise à jour ne constitue pas une nouvelle version de
Debian <release> mais seulement une mise à jour de certains des paquets qu'elle
contient. Il n'est pas nécessaire de jeter les anciens médias de la
version <codename>. Après installation, les paquets peuvent être mis à niveau
vers les versions courantes en utilisant un miroir Debian à jour.
</p>

<p>
Ceux qui installent fréquemment les mises à jour à partir de security.debian.org
n'auront pas beaucoup de paquets à mettre à jour et la plupart des mises à jour
de security.debian.org sont comprises dans cette mise à jour.
</p>

<p>
De nouvelles images d'installation seront prochainement disponibles à leurs
emplacements habituels.
</p>

<p>
Mettre à jour une installation vers cette révision peut se faire en faisant
pointer le système de gestion de paquets sur l'un des nombreux miroirs HTTP de
Debian. Une liste complète des miroirs est disponible à l'adresse :
</p>

<div class="center">
  <a href="$(HOME)/mirror/list">https://www.debian.org/mirror/list</a>
</div>




<h2>Corrections de bogues divers</h2>

<p>
Cette mise à jour de la version oldstable apporte quelques corrections importantes
aux paquets suivants :
</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction adduser "Correction d'une vulnérabilité d'injection de commande dans deluser">
<correction aide "Correction de la gestion des attributs étendus dans les liens symboliques">
<correction amd64-microcode "Mise à jour du microcode inclus, y compris des corrections pour <q>AMD Inception</q> pour les processeurs AMD Zen4 [CVE-2023-20569]">
<correction appstream-glib "Gestion des étiquettes &lt;em&gt; et &lt;code&gt; dans les métadonnées">
<correction asmtools "Rétroportage pour Bullseye pour les constructions futures d'openjdk-11">
<correction autofs "Correction du déverrouillage de mutex absent ; pas d'utilisation de rpcbind pour les montages NFS4 ; correction d'une régression affectant l'accessibilité des hôtes double couche">
<correction base-files "Mise à jour pour la version 11.8">
<correction batik "Correction de problèmes de contrefaçon de requête côté serveur [CVE-2022-44729 CVE-2022-44730]">
<correction bmake "Conflit avec bsdowl (&lt;&lt; 2.2.2-1.2~) pour assurer des mises à niveau sans problème">
<correction boxer-data "Rétroportage de corrections de compatibilité avec Thunderbird">
<correction ca-certificates-java "Contournement de la non configuration de JRE lors des nouvelles installations">
<correction cairosvg "Gestion de données : URL en mode sûr">
<correction cargo-mozilla "Nouvelle version <q>amont</q>, pour prendre en charge la construction des versions récentes de firefox-esr">
<correction clamav "Nouvelle version amont stable ; correction d'une vulnérabilité de déni de service au moyen de l'analyseur HFS+ [CVE-2023-20197]">
<correction cpio "Correction d'un problème d'exécution de code arbitraire [CVE-2021-38185] ; replacement de Suggests : libarchive1 par libarchive-dev">
<correction cryptmount "Correction de l'initialisation de la mémoire dans l'analyseur de ligne de commande">
<correction cups "Correction de problèmes de dépassement de tampon de tas [CVE-2023-4504 CVE-2023-32324], d'un problème d'accès non authentifié [CVE-2023-32360], d'un problème d'utilisation de mémoire après libération [CVE-2023-34241]">
<correction curl "Correction de problèmes d'exécution de code [CVE-2023-27533 CVE-2023-27534], de problèmes de divulgation d'information [CVE-2023-27535 CVE-2023-27536 CVE-2023-28322], d'un problème de réutilisation inappropriée de connexion [CVE-2023-27538], d'un problème de validation incorrecte de certificat [CVE-2023-28321]">
<correction dbus "Nouvelle version amont stable ; correction d'un problème de déni de service [CVE-2023-34969]">
<correction debian-design "Reconstruction avec la nouvelle version de boxer-data">
<correction debian-installer "Passage de l'ABI du noyau Linux à la version 5.10.0-26 ; reconstruction avec proposed-updates">
<correction debian-installer-netboot-images "Reconstruction avec proposed-updates">
<correction debian-parl "Reconstruction avec la nouvelle version de boxer-data">
<correction debian-security-support "Réglage DEB_NEXT_VER_ID=12 comme Bookworm est la version suivante ; security-support-limited : ajout de gnupg1">
<correction distro-info-data "Ajout de Debian 14 <q>Forky</q> ; correction de la date de publication d'Ubuntu 23.04 ; ajout d'Ubuntu 23.10 Mantic Minotaur ; ajout de la date prévue de publication de Debian Bookworm">
<correction dkimpy "Nouvelle version amont de correction de bogues">
<correction dpdk "Nouvelle version amont stable">
<correction dpkg "Ajout de la prise en charge des processeurs loong64 ; gestion de l'absence de Version lors du formatage de source:Upstream-Version ; correction d'une fuite de mémoire de varbuf dans pkg_source_version()">
<correction flameshot "Désactivation des téléversements vers imgur par défaut ; correction du nom du fichier d/NEWS dans le téléversement précédent">
<correction ghostscript "Correction d'un problème de dépassement de tampon [CVE-2023-38559] ; essai et sécurisation du démarrage du serveur IJS [CVE-2023-43115]">
<correction gitit "Reconstruction avec la nouvelle version de pandoc">
<correction grunt "Correction d'une situation de compétition pendant la copie de liens symboliques [CVE-2022-1537]">
<correction gss "Ajour de Breaks+Replaces : libgss0 (&lt;&lt; 0.1)">
<correction haskell-hakyll "Reconstruction avec la nouvelle version de pandoc">
<correction haskell-pandoc-citeproc "Reconstruction avec la nouvelle version de pandoc">
<correction hnswlib "Correction d'une double libération de mémoire dans init_index quand l'argument M est un grand entier [CVE-2023-37365]">
<correction horizon "Correction d'un problème de redirection ouverte [CVE-2022-45582]">
<correction inetutils "Vérification des valeurs de retour pour les fonctions set*id() pour éviter d'éventuels problèmes de sécurité [CVE-2023-40303]">
<correction krb5 "Correction de libération d'un pointeur non initialisé [CVE-2023-36054]">
<correction kscreenlocker "Correction d'une erreur d'authentification lors de l'utilisation de PAM">
<correction lacme "Gestion correcte des états de certificats d'autorité <q>ready</q>, <q>processing</q> et <q>valid</q>">
<correction lapack "Correction des vecteurs propres de matrice ">
<correction lemonldap-ng "Correction d'une redirection ouverte quand OIDC RP n'a pas d'URI de redirection ; correction d'un problème de contrefaçon de requête côté serveur [CVE-2023-44469] ; correction d'une redirection ouverte due au traitement incorrect d'un échappement">
<correction libapache-mod-jk "Suppression de la fonctionnalité de mappage implicite qui pouvait mener à l'exposition imprévue de l'état <q>worker</q> et/ou <q>bypass</q> des contraintes de sécurité [CVE-2023-41081]">
<correction libbsd "Correction de boucle infinie dans MD5File">
<correction libclamunrar "Nouvelle version amont stable">
<correction libprelude "Module Python rendu utilisable">
<correction libreswan "Correction d'un problème de déni de service [CVE-2023-30570]">
<correction libsignal-protocol-c "Correction d'un problème de dépassement d'entier [CVE-2022-48468]">
<correction linux "Nouvelle version amont stable">
<correction linux-signed-amd64 "Nouvelle version amont stable">
<correction linux-signed-arm64 "Nouvelle version amont stable">
<correction linux-signed-i386 "Nouvelle version amont stable">
<correction logrotate "Remplacement évité de /dev/null par un fichier ordinaire lorsqu'utilisé pour le fichier d'état">
<correction ltsp "Utilisation évitée de <q>mv</q> d'un lien symbolique d'init afin de contourner un problème d'overlayfs">
<correction lttng-modules "Correction de problèmes de construction avec les versions récentes du noyau">
<correction lua5.3 "Correction d'utilisation de mémoire après libération dans lua_upvaluejoin (lapi.c) [CVE-2019-6706] ; correction d'une erreur de segmentation dans getlocal et setlocal (ldebug.c) [CVE-2020-24370]">
<correction mariadb-10.5 "Nouvelle version amont de correction de bogues [CVE-2022-47015]">
<correction mujs "Correction de sécurité">
<correction ncurses "Chargement refusé d'entrées terminfo personnalisées dans les programmes setuid ou setgid [CVE-2023-29491]">
<correction node-css-what "Correction d'un problème de déni de service basé sur les expressions rationnelles [CVE-2022-21222 CVE-2021-33587]">
<correction node-json5 "Correction d'un problème de pollution de prototype [CVE-2022-46175]">
<correction node-tough-cookie "Correction de sécurité : pollution de prototype [CVE-2023-26136]">
<correction nvidia-graphics-drivers "Nouvelle version amont [CVE-2023-25515 CVE-2023-25516] ; amélioration de la compatibilité avec les noyaux récents">
<correction nvidia-graphics-drivers-tesla-450 "Nouvelle version amont [CVE-2023-25515 CVE-2023-25516]">
<correction nvidia-graphics-drivers-tesla-470 "Nouvelle version amont de correction de bogues [CVE-2023-25515 CVE-2023-25516]">
<correction openblas "Correction des résultats de DGEMM sur du matériel compatible AVX512, quand le paquet a été construit sur du matériel antérieur à AVX2">
<correction openssh "Correction d'un problème d'exécution de code à distance au moyen d'un socket d'agent transmis [CVE-2023-38408]">
<correction openssl "Nouvelle version amont stable ; correction de problèmes de déni de service [CVE-2023-3446 CVE-2023-3817]">
<correction org-mode "Correction d'une vulnérabilité d'injection de commande [CVE-2023-28617]">
<correction pandoc "Correction de problèmes d'écriture de fichiers arbitraires [CVE-2023-35936 CVE-2023-38745]">
<correction pev "Correction d'un problème de dépassement de tampon [CVE-2021-45423]">
<correction php-guzzlehttp-psr7 "Correction d'une validation d'entrée incorrecte [CVE-2023-29197]">
<correction php-nyholm-psr7 "Correction d'un problème de validation d'entrée incorrecte [CVE-2023-29197]">
<correction postgis "Correction d'une régression dans l'ordre des axes">
<correction protobuf "Corrections de sécurité : déni de service dans Java [CVE-2021-22569] ; déréférencement de pointeur NULL [CVE-2021-22570] ; déni de service de mémoire [CVE-2022-1941]">
<correction python2.7 "Correction d'un problème de <q>masquage de paramètre</q> [CVE-2021-23336], d'un problème d'injection d'URL [CVE-2022-0391], d'un problème d'utilisation de mémoire après libération [CVE-2022-48560], d'un problème d'entité externe XML [CVE-2022-48565] ; amélioration des comparaisons de temps constant dans compare_digest() [CVE-2022-48566] ; amélioration de l'analyse d'URL [CVE-2023-24329] ; lecture interdite de données non authentifiées sur un SSLSocket [CVE-2023-40217]">
<correction qemu "Correction de boucle infinie [CVE-2020-14394], d'un problème de déréférencement de pointeur NULL [CVE-2021-20196], d'un problème de dépassement d'entier [CVE-2021-20203], de problèmes de dépassement de tampon [CVE-2021-3507 CVE-2023-3180], de problèmes de déni de service [CVE-2021-3930 CVE-2023-3301], d'un problème d'utilisation de mémoire après libération [CVE-2022-0216], de possibles problèmes de dépassement de tas et d'utilisation de mémoire après libération, [CVE-2023-0330], d'un problème de lecture hors limites [CVE-2023-1544]">
<correction rar "Nouvelle version amont ; correction d'un problème de traversée de répertoire [CVE-2022-30333] ; correction d'un problème d'exécution de code arbitraire [CVE-2023-40477]">
<correction rhonabwy "Correction de dépassement de tampon d'aesgcm [CVE-2022-32096]">
<correction roundcube "Nouvelle version amont stable ; correction d'un problème de script intersite [CVE-2023-43770] ; Enigma : Correction de la synchronisation initiale des clés privées">
<correction rust-cbindgen "Nouvelle version <q>amont</q> pour prendre en charge la construction des versions récentes de firefox-esr">
<correction rustc-mozilla "Nouvelle version <q>amont</q> pour prendre en charge la construction des versions récentes de firefox-esr">
<correction schleuder "Ajout d'une dépendance versionnée à ruby-activerecord">
<correction sgt-puzzles "Correction de divers problèmes de sécurité dans le chargement de jeux [CVE-2023-24283 CVE-2023-24284 CVE-2023-24285 CVE-2023-24287 CVE-2023-24288 CVE-2023-24291]">
<correction spip "Plusieurs corrections de sécurité ; correction de sécurité pour le filtrage étendu de données d'authentification">
<correction spyder "Correction d'un correctif cassé lors d'une mise à jour précédente">
<correction systemd "Udev : correction de la création de liens symboliques /dev/serial/by-id/ symlinks pour les périphériques USB ; correction de fuite de mémoire dans daemon-reload ; correction d'un blocage de calcul des spécifications de calendrier lors du changement d'heure d'été quand fuseau horaire = Europe/Dublin">
<correction tang "Correction d'une situation de compétition lors de la création ou la rotation des clés ; assertion de permissions restrictives sur le répertoire de clés [CVE-2023-1672] ; tangd-rotate-keys rendu exécutable">
<correction testng7 "Rétroportage dans stable pour les constructions futures d'openjdk-17">
<correction tinyssh "Contournement des paquets entrant qui ne respectent pas la longueur de paquet maximale de paquet">
<correction unrar-nonfree "Correction d'un problème d'écrasement de fichier [CVE-2022-48579] ; correction d'un problème d'exécution de code à distance [CVE-2023-40477]">
<correction xen "Nouvelle version amont stable ; corrections de problèmes de sécurité [CVE-2023-20593 CVE-2023-20569 CVE-2022-40982]">
<correction yajl "Correction de sécurité de fuite de mémoire ; corrections de sécurité : déni de service potentiel grâce à un fichier JSON contrefait [CVE-2017-16516] ; corruption de mémoire de tas lors de traitement de grandes entrées (~2Go) [CVE-2022-24795] ; correction d'un correctif incomplet pour CVE-2023-33460">
</table>


<h2>Mises à jour de sécurité</h2>


<p>
Cette révision ajoute les mises à jour de sécurité suivantes à la version
oldstable. L'équipe de sécurité a déjà publié une annonce pour chacune de ces
mises à jour :
</p>

<table border=0>
<tr><th>Identifiant</th>  <th>Paquet</th></tr>
<dsa 2023 5394 ffmpeg>
<dsa 2023 5395 nodejs>
<dsa 2023 5396 evolution>
<dsa 2023 5396 webkit2gtk>
<dsa 2023 5397 wpewebkit>
<dsa 2023 5398 chromium>
<dsa 2023 5399 odoo>
<dsa 2023 5400 firefox-esr>
<dsa 2023 5401 postgresql-13>
<dsa 2023 5402 linux-signed-amd64>
<dsa 2023 5402 linux-signed-arm64>
<dsa 2023 5402 linux-signed-i386>
<dsa 2023 5402 linux>
<dsa 2023 5403 thunderbird>
<dsa 2023 5404 chromium>
<dsa 2023 5405 libapache2-mod-auth-openidc>
<dsa 2023 5406 texlive-bin>
<dsa 2023 5407 cups-filters>
<dsa 2023 5408 libwebp>
<dsa 2023 5409 libssh>
<dsa 2023 5410 sofia-sip>
<dsa 2023 5411 gpac>
<dsa 2023 5412 libraw>
<dsa 2023 5413 sniproxy>
<dsa 2023 5414 docker-registry>
<dsa 2023 5415 libreoffice>
<dsa 2023 5416 connman>
<dsa 2023 5417 openssl>
<dsa 2023 5418 chromium>
<dsa 2023 5419 c-ares>
<dsa 2023 5420 chromium>
<dsa 2023 5421 firefox-esr>
<dsa 2023 5422 jupyter-core>
<dsa 2023 5423 thunderbird>
<dsa 2023 5424 php7.4>
<dsa 2023 5426 owslib>
<dsa 2023 5427 webkit2gtk>
<dsa 2023 5428 chromium>
<dsa 2023 5430 openjdk-17>
<dsa 2023 5431 sofia-sip>
<dsa 2023 5432 xmltooling>
<dsa 2023 5433 libx11>
<dsa 2023 5434 minidlna>
<dsa 2023 5435 trafficserver>
<dsa 2023 5436 hsqldb1.8.0>
<dsa 2023 5437 hsqldb>
<dsa 2023 5438 asterisk>
<dsa 2023 5439 bind9>
<dsa 2023 5440 chromium>
<dsa 2023 5441 maradns>
<dsa 2023 5442 flask>
<dsa 2023 5443 gst-plugins-base1.0>
<dsa 2023 5444 gst-plugins-bad1.0>
<dsa 2023 5445 gst-plugins-good1.0>
<dsa 2023 5446 ghostscript>
<dsa 2023 5447 mediawiki>
<dsa 2023 5449 webkit2gtk>
<dsa 2023 5450 firefox-esr>
<dsa 2023 5451 thunderbird>
<dsa 2023 5452 gpac>
<dsa 2023 5453 linux-signed-amd64>
<dsa 2023 5453 linux-signed-arm64>
<dsa 2023 5453 linux-signed-i386>
<dsa 2023 5453 linux>
<dsa 2023 5455 iperf3>
<dsa 2023 5456 chromium>
<dsa 2023 5457 webkit2gtk>
<dsa 2023 5459 amd64-microcode>
<dsa 2023 5461 linux-signed-amd64>
<dsa 2023 5461 linux-signed-arm64>
<dsa 2023 5461 linux-signed-i386>
<dsa 2023 5461 linux>
<dsa 2023 5463 thunderbird>
<dsa 2023 5464 firefox-esr>
<dsa 2023 5465 python-django>
<dsa 2023 5467 chromium>
<dsa 2023 5468 webkit2gtk>
<dsa 2023 5470 python-werkzeug>
<dsa 2023 5471 libhtmlcleaner-java>
<dsa 2023 5472 cjose>
<dsa 2023 5473 orthanc>
<dsa 2023 5474 intel-microcode>
<dsa 2023 5475 linux-signed-amd64>
<dsa 2023 5475 linux-signed-arm64>
<dsa 2023 5475 linux-signed-i386>
<dsa 2023 5475 linux>
<dsa 2023 5476 gst-plugins-ugly1.0>
<dsa 2023 5478 openjdk-11>
<dsa 2023 5479 chromium>
<dsa 2023 5480 linux-signed-amd64>
<dsa 2023 5480 linux-signed-arm64>
<dsa 2023 5480 linux-signed-i386>
<dsa 2023 5480 linux>
<dsa 2023 5481 fastdds>
<dsa 2023 5482 tryton-server>
<dsa 2023 5483 chromium>
<dsa 2023 5484 librsvg>
<dsa 2023 5485 firefox-esr>
<dsa 2023 5486 json-c>
<dsa 2023 5487 chromium>
<dsa 2023 5489 file>
<dsa 2023 5490 aom>
<dsa 2023 5491 chromium>
<dsa 2023 5493 open-vm-tools>
<dsa 2023 5494 mutt>
<dsa 2023 5495 frr>
<dsa 2023 5497 libwebp>
<dsa 2023 5500 flac>
<dsa 2023 5502 xorgxrdp>
<dsa 2023 5502 xrdp>
<dsa 2023 5503 netatalk>
<dsa 2023 5504 bind9>
<dsa 2023 5505 lldpd>
<dsa 2023 5507 jetty9>
<dsa 2023 5510 libvpx>
</table>


<h2>Paquets supprimés</h2>

<p>Les paquets suivants ont été supprimés à cause de circonstances hors de
notre contrôle :</p>

<table border=0>
<tr><th>Paquet</th>               <th>Raison</th></tr>
<correction atlas-cpp "Version amont instable, inappropriée pour Debian">
<correction ember-media "Version amont instable, inappropriée pour Debian">
<correction eris "Version amont instable, inappropriée pour Debian">
<correction libwfut "Version amont instable, inappropriée pour Debian">
<correction mercator "Version amont instable, inappropriée pour Debian">
<correction nomad "Plus de correctif de sécurité disponible">
<correction nomad-driver-lxc "Dépend de nomad qui doit être retiré">
<correction skstream "Version amont instable, inappropriée pour Debian">
<correction varconf "Version amont instable, inappropriée pour Debian">
<correction wfmath "Version amont instable, inappropriée pour Debian">

</table>

<h2>Installateur Debian</h2>
<p>
L'installateur a été mis à jour pour inclure les correctifs incorporés dans
cette version de oldstable.
</p>

<h2>URL</h2>

<p>
Liste complète des paquets qui ont été modifiés dans cette version :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/<downcase <codename>>/ChangeLog">
</div>

<p>
Adresse de l'actuelle distribution oldstable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable/">
</div>

<p>
Mises à jour proposées à la distribution oldstable :
</p>

<div class="center">
  <url "https://deb.debian.org/debian/dists/oldstable-proposed-updates">
</div>

<p>
Informations sur la distribution oldstable (notes de publication, <i>errata</i>, etc.) :
</p>

<div class="center">
  <a
  href="$(HOME)/releases/oldstable/">https://www.debian.org/releases/oldstable/</a>
</div>

<p>
Annonces et informations de sécurité :
</p>

<div class="center">
  <a href="$(HOME)/security/">https://www.debian.org/security/</a>
</div>

<h2>À propos de Debian</h2>
<p>
Le projet Debian est une association de développeurs de logiciels libres qui
offrent volontairement leur temps et leurs efforts pour produire le système
d'exploitation complètement libre Debian.
</p>

<h2>Contacts</h2>

<p>
Pour de plus amples informations, veuillez consulter le site Internet de
Debian <a href="$(HOME)/">https://www.debian.org/</a> ou envoyez un courrier
électronique à &lt;press@debian.org&gt; ou contactez l'équipe de publication de
la version stable à &lt;debian-release@lists.debian.org&gt;.</p>



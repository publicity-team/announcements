# Status: [frozen]
# $Id$
# $Rev$
<define-tag pagetitle>Debian 7 Long Term Support chega ao fim do seu ciclo de vida</define-tag>
<define-tag release_date>2018-06-01</define-tag>
#use wml::debian::news

##
## Translators: 
## - if translating while the file is in publicity-team/announcements repo,
##   please ignore the translation-check header. Publicity team will add it
##   including the correct translation-check header when moving the file 
##   to the web repo
##
## - if translating while the file is in webmaster-team/webwml repo,
##   please use the copypage.pl script to create the page in your language
##   subtree including the correct translation-check header.
##

<p>A equipa Debian Long Term Support (LTS) anuncia que o suporte para o Debian 7
"Wheezy" chegou ao fim da sua vida a 31 de Maio de 2018,
cinco anos depois do início do seu lançamento em 4 de Maio de 2013.</p>

<p>Debian não fornecerá mais actualizações de segurança para o Debian 7. Um 
sub-conjunto de pacotes Wheezy será suportado por entidades externas. Informação
detalhada pode ser encontrada em <a href="https://wiki.debian.org/LTS/Extended">
Extended LTS</a>.</p>

<p>A equipa LTS irá preparar a transição para o Debian 8 "Jessie", sendo esta
versão a actual oldstable. A equipa LTS assegurará o suporte a partir de 17 de 
Junho de 2018.</p>

<p>Debian 8 receberá também suporte de Longo Termo durante cinco anos após o seu 
lançamento, terminando a 30 de Junho de 2020. As arquitecturas suportadas 
incluem amd64, i386, armel and armhf.</p>

<p>Para mais informações de como usar o Jessie LTS e para actualizar a partir do
Wheezy LTS, veja por favor <a href="https://wiki.debian.org/LTS/Using">
LTS/Using</a>.</p>

<p>Debian e a sua equipa LTS gostariam de agradecer a todos os utilizadores que 
contribuíram, developers e patrocinadores que tornaram possível a extensão da 
vida dos anteriores lançamentos estáveis, e a todos os que tornaram esta LTS um 
sucesso.</p>

<p>Se confia na Debian LTS, considere por favor 
<a href="https://wiki.debian.org/LTS/Development">juntar-se à equipa</a>,
fornecendo patches, testando ou 
<a href="https://wiki.debian.org/LTS/Funding">financiando os esforços</a>.</p>

<h2>Sobre Debian</h2>

<p>
O Projecto Debian foi fundado em 1993 por Ian Murdock para ser um projecto 
de uma comunidade verdadeiramente livre. Desde então o projecto cresceu para se 
tornar um dos maiores e mais influentes projectos de código aberto. Milhares de 
voluntários de todo o mundo trabalham em conjunto para criar e manter software 
Debian. Disponível em 70 idiomas e suportando uma variedade enorme de tipos de 
computadores, Debian define-se a si próprio como o <q>sistema operativo 
universal</q>.
</p>

<h2>Mais Informação</h2>
<p>Mais informação sobre o Debian Long Term Support pode ser encontrada em 
<a href="https://wiki.debian.org/LTS/">https://wiki.debian.org/LTS/</a>.</p>

<h2>Informação de contacto</h2>

<p>Para mais informação, visite por favor as páginas web Debian em 
<a href="$(HOME)/">https://www.debian.org/</a> ou envie um mail para 
&lt;press@debian.org&gt;.</p>
# Status: [open-for-edit]
# $Id$
# $Rev$

<define-tag pagetitle>Debian boosts reliability of our core infrastructure with Lenovo servers</define-tag>

<define-tag release_date>2024-10-28</define-tag>
#use wml::debian::news
##
## Translators should uncomment the following line, add their name and
## the translation-check hash of the english version added to Debian's
## webwml repository
# ← this one must be removed; not that one → #use wml::debian::translation-check translation="XXXXX" maintainer="NAME"

<p>
Over the last several months, the Debian System Administration (DSA) team
has been upgrading hardware in our various locations. Thanks to the
generous preferential prices that Lenovo have extended to Debian, the team
has updated Debian equipment at the University of British Columbia (UBC),
our hosting partner in Vancouver, Canada. DSA have commissioned the Lenovo
servers into a new cluster at UBC and have migrated the virtual machines
from the old cluster to the new cluster.
</p>

<p>
<a href="https://www.lenovo.com/">Lenovo</a>, a long-time sponsor of Debian
Conferences, has recently provided preferential pricing for their servers
in support of Debian core infrastructure. It is only through the donation
of volunteer effort, in-kind equipment and services, and funding that
Debian is able to deliver on our commitment to a free operating system. We
are very appreciative of Lenovo's generosity.
</p>

#(sample quote - replace with material from Lenovo)
#"Our partnership with Debian aligns with our strategy to expand hardware
#support for opensource operating systems etc etc etc.," said someone with
#Lenovo.

<p>
"Lenovo's generosity allows Debian to refresh the equipment that underpins
a number of our core services," explained Luca Filipozzi, member of the
Debian System Administration (DSA) team. "In particular, Debian's core
source code management, continuous integration, and continuous deployment
services are now running on the Lenovo cluster. The new ThinkSystem
SR635 V3 servers that we acquired are very performant and they have already
allowed us to address the performance challenges that we were having with
our services. It would not be possible to maintain our services without
the generosity of partners such as Lenovo. Additionally, our experience
with Lenovo sales, engineering, and support has been excellent. Would
recommend, as they say. Thank you, Lenovo."
</p>

<h2>About Lenovo</h2>

<p>
Lenovo is a global technology leader manufacturing a wide portfolio of
connected products, including smartphones, tablets, PCs and workstations
as well as AR/VR devices, smart home/office and data center solutions.
Lenovo understands how critical open systems and platforms are to a
connected world.
</p>

<h2>About Debian</h2>

<p>
The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.
</p>

<h2>Contact Information</h2>

<p>
For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;<a href="mailto:press@debian.org">press@debian.org</a>&gt;.
</p>

<define-tag pagetitle>Debian Edu / Skolelinux Buster — a complete Linux solution for your school</define-tag>
<define-tag release_date>2019-07-07</define-tag>
#use wml::debian::news

## Translators should uncomment the following line and add their name
## Leaving translation at 1.1 is okay; that's the first version which will
## be added to Debian's webwml repository
##
# ← this one must be removed; not that one → #use wml::debian::translation-check translation="1.1" maintainer=""

<p>
Do you have to administrate a computer lab or a whole school network?
Would you like to install servers, workstations and laptops which
will then work together?
Do you want the stability of Debian with network services already
preconfigured?
Do you wish to have a web-based tool to manage systems and several
hundred or even more user accounts?
Have you asked yourself if and how older computers could be used?
</p>

<p>
Then Debian Edu is for you. The teachers themselves or their technical support
can roll out a complete multi-user multi-machine study environment within
a few days. Debian Edu comes with hundreds of applications pre-installed,
but you can always add more packages from Debian.
</p>

<p>
The Debian Edu developer team is happy to announce Debian Edu 10
<q>Buster</q>, the Debian Edu / Skolelinux release based
on the Debian 10 <q>Buster</q> release.
Please consider testing it and reporting back (&lt;debian-edu@lists.debian.org&gt;)
to help us to improve it further.
</p>

<h2>About Debian Edu and Skolelinux</h2>

<p>
<a href="https://wiki.debian.org/DebianEdu"> Debian Edu, also known as
Skolelinux</a>, is a Linux distribution based on Debian providing an
out-of-the box environment of a completely configured school network.
Immediately after installation a school server running all services
needed for a school network is set up just waiting for users and
machines to be added via GOsa², a comfortable web interface.
A netbooting environment is prepared, so after initial
installation of the main server from CD / DVD / BD or USB stick all other
machines can be installed via the network.
Older computers (even up to ten or so years old) can be used as LTSP
thin clients or diskless workstations, booting from the network without
any installation and configuration at all.
The Debian Edu school server provides an LDAP database and Kerberos
authentication service, centralized home directories, a DHCP server, a web
proxy and many other services. The desktop contains more than 60 educational
software packages and more are available from the Debian archive.
Schools can choose between the desktop environments Xfce, GNOME, LXDE,
MATE, KDE Plasma and LXQt.
</p>

<h2>New features for Debian Edu 10 <q>Buster</q></h2>

<p>These are some items from the release notes for Debian Edu 10 <q>Buster</q>,
based on the Debian 10 <q>Buster</q> release.
The full list including more detailed information is part of the related
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Features#New_features_in_Debian_Edu_Buster">Debian Edu manual chapter</a>.
</p>

<ul>
<li>
Official Debian installation images are now available.
</li>
<li>
Site specific modular installation is possible.
</li>
<li>
Additional meta-packages grouping educational packages by school level are
provided.
</li>
<li>
Improved desktop localization for all languages Debian supports.
</li>
<li>
Tool available to ease setting up site specific multi-language support.
</li>
<li>
GOsa²-Plugin Password Management has been added.
</li>
<li>
Improved TLS/SSL support inside the internal network.
</li>
<li>
The Kerberos setup supports NFS and SSH services.
</li>
<li>
A tool to re-generate the LDAP database is available.
</li>
<li>
X2Go server is installed on all systems with profile LTSP-Server.
</li>
</ul>

<h2>Download options, installation steps and manual</h2>

<p>
Separate Network-Installer CD images for 64-bit and 32-bit PCs
are available. Only in rare cases (for PCs older than around 12 years) the
32-bit image will be needed. The images can be downloaded at the following
locations:
</p>
<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-cd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-cd/>
</li>
</ul>

<p>
Alternatively extended BD images (more than 5 GB large) are
also available. It is possible to set up a whole Debian Edu network without an 
Internet connection (all supported desktop environments, all languages supported
by Debian). These images can be downloaded at the following locations: 
</p>

<ul>
<li>
<url http://get.debian.org/cdimage/release/current/amd64/iso-bd/>
</li>
<li>
<url http://get.debian.org/cdimage/release/current/i386/iso-bd/>
</li>
</ul>

<p>
The images can be verified using the signed checksums provided in the download
directory.
<br />
Once you've downloaded an image, you can check that
<ul>
<li>
its checksum matches that expected from the checksum file; and that
</li>
<li>
the checksum file has not been tampered with.
</li>
</ul>
For more information about how to do these steps, read the
<a href="https://www.debian.org/CD/verify">verification guide</a>.
</p>

<p>
Debian Edu 10 <q>Buster</q> is entirely based on Debian 10 <q>Buster</q>; so the
sources for all packages are available from the Debian archive.
</p>

<p>
Please note the
<a href="https://wiki.debian.org/DebianEdu/Status/Buster">Debian Edu Buster status page</a>.
for up-to-date information about Debian Edu 10 <q>Buster</q> including
instructions how to use <code>rsync</code> for downloading the ISO images.
</p>

<p>
When upgrading from Debian Edu 9 <q>Stretch</q> please read the related
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Upgrades">Debian Edu manual chapter</a>.
</p>

<p>
For installation notes please read the related
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/Installation#Installing_Debian_Edu">Debian Edu manual chapter</a>.
</p>

<p>
After installation you need to take these
<a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/GettingStarted">first steps</a>.
</p>

<p>
Please see the <a href="https://wiki.debian.org/DebianEdu/Documentation/Buster/">Debian Edu wiki pages</a>
for the latest English version of the Debian Edu <q>Buster</q> manual.
The manual has been fully translated to German, French, Italian, Danish, Dutch,
Norwegian Bokmål and Japanese. Partly translated versions exist for Spanish
and Simplified Chinese.
An overview of <a href="https://jenkins.debian.net/userContent/debian-edu-doc/">
the latest translated versions of the manual</a> is available.
</p>

<p>
More information about Debian 10 <q>Buster</q> itself is provided in the release
notes and the installation manual; see <a href="$(HOME)/">https://www.debian.org/</a>.
</p>

<h2>About Debian</h2>

<p>The Debian Project is an association of Free Software developers who
volunteer their time and effort in order to produce the completely free
operating system Debian.</p>

<h2>Contact Information</h2>

<p>For further information, please visit the Debian web pages at
<a href="$(HOME)/">https://www.debian.org/</a> or send mail to
&lt;press@debian.org&gt;.</p>

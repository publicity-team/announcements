#use wml::debian::translation-check translation="5e952a33ef16a6a69c339dc8b42c5ff146d447e9"
<define-tag pagetitle>Debian 12 <q>bookworm</q> lançado</define-tag>
<define-tag release_date>2023-06-10</define-tag>
#use wml::debian::news

<p>Após 1 ano, 9 meses e 28 dias de desenvolvimento, o projeto Debian
tem o orgulho de apresentar sua nova versão estável (stable) 12 (codinome <q>bookworm</q>).</p>

<p>O <q>bookworm</q> terá suporte pelos próximos 5 anos graças ao
trabalho combinado da <a href="https://security-team.debian.org/">equipe de segurança do Debian</a>
e pela <a href="https://wiki.debian.org/LTS">equipe de suporte de longo prazo do Debian.</a></p>

<p>Seguindo a <a href="$(HOME)/vote/2022/vote_003">Resolução Geral de 2022 sobre firmware não livre</a>,
introduzimos uma nova área no repositório, tornando possível separar os firmwares
não livres dos outros pacotes não livres:
<ul>
<li>non-free-firmware</li>
</ul>

A maioria dos pacotes de firmwares não livres foram movidos de <b>non-free</b>
para <b>non-free-firmware</b>. Essa separação permite construir uma variedade
de imagens oficiais de instalação.
</p>

<p>O Debian 12 <q>bookworm</q> vem com vários ambientes de área de trabalho, como:
</p>
<ul>
<li>Gnome 43,</li>
<li>KDE Plasma 5.27,</li>
<li>LXDE 11,</li>
<li>LXQt 1.2.0,</li>
<li>MATE 1.26,</li>
<li>Xfce 4.18</li>
</ul>

<p>Este lançamento contém mais de <b>11.089</b> novos pacotes para uma contagem total de <b>64.419</b>
pacotes, enquanto mais de <b>6.296</b> pacotes foram removidos como <q>obsoletos</q>. <b>43.254</b>
pacotes foram atualizados nesta versão.

São necessários <b>365.016.420 kB (365 GB)</b> de espaço total de disco para armazenar o <q>bookworm</q>,
o qual é composto de <b>1.341.564.204</b> linhas de código.</p>

<p>O <q>bookworm</q> tem mais páginas de manual traduzidas do que nunca, graças aos(às) nossos(as)
tradutores(as) que disponibilizaram <b>man</b>-pages (páginas de manual) em vários
idiomas como: tcheco, dinamarquês, grego, finlandês, indonésio, macedônio,
norueguês (Bokmål), russo, sérvio, sueco, ucraniano e vietnamita.
Todas as man pages do <b>systemd</b> agora estão totalmente disponíveis em
alemão.</p>

<p>O Blend Debian Med apresenta um novo pacote: <b>shiny-server</b> que
simplifica aplicações web científicas usando <b>R</b>. Mantivemos nossos
esforços de fornecer suporte de integração contínua para os pacotes da equipe
Debian Med. Instale os metapacotes na versão 3.8.x para o Debian bookworm.</p>

<p>O Blend Debian Astro continua fornecendo uma solução única para
astrônomos profissionais, entusiastas e amadores com atualizações para quase
todas as versões dos pacotes de software no blend. <b>astap</b> e
<b>planetary-system-stacker</b> ajudam com empilhamento de imagens e resolução
astrométrica. <b>openvlbi</b>, o correlacionador de código aberto, agora é
incluído.</p>

<p>Suporte para inicialização segura (Secure Boot) em ARM64 foi reintroduzido: usuários(as) com
hardware ARM64 compatível com UEFI podem inicializar com o modo Secure Boot
habilitado para aproveitar ao máximo este recurso de segurança.</p>

<p>O Debian 12 <q>bookworm</q> inclui vários pacotes de software atualizados
(mais de 67% de todos os pacotes da versão anterior), como:

<ul>
<li>Apache 2.4.57</li>
<li>BIND DNS Server 9.18</li>
<li>Cryptsetup 2.6</li>
<li>Dovecot MTA 2.3.19</li>
<li>Emacs 28.2</li>
<li>Exim (servidor de e-mail padrão) 4.96</li>
<li>GIMP 2.10.34</li>
<li>GNU Compiler Collection 12.2</li>
<li>GnuPG 2.2.40</li>
<li>Inkscape 1.2.2</li>
<li>The GNU C Library (biblioteca GNU para linguagem C) 2.36</li>
<li>lighthttpd 1.4.69</li>
<li>LibreOffice 7.4</li>
<li>Séries do Linux kernel 6.1</li>
<li>LLVM/Clang toolchain 13.0.1, 14.0 (padrão), e 15.0.6</li>
<li>MariaDB 10.11</li>
<li>Nginx 1.22</li>
<li>OpenJDK 17</li>
<li>OpenLDAP 2.5.13</li>
<li>OpenSSH 9.2p1</li>
<li>Perl 5.36</li>
<li>PHP 8.2</li>
<li>Postfix MTA 3.7</li>
<li>PostgreSQL 15</li>
<li>Python 3, 3.11.2</li>
<li>Rustc 1.63</li>
<li>Samba 4.17</li>
<li>systemd 252</li>
<li>Vim 9.0</li>
</ul>
</p>

<p>
Com esta ampla seleção de pacotes e seu tradicionalmente vasto suporte a
arquiteturas, o Debian mais uma vez permanece fiel ao seu objetivo de ser
<q>o sSistema operacional universal</q>. Ele é adequado para diferentes casos de uso:
de sistemas desktop a netbooks; de servidores de desenvolvimento a sistemas
de cluster; e para servidores de bancos de dados, web e de armazenamento. Ao mesmo
tempo, esforços adicionais de garantia de qualidade, como testes de instalação e
atualização automáticos para todos os pacotes no repositório Debian, asseguram que o
<q>bookworm</q> alcance as altas expectativas que os(as) usuários(as) tem de uma versão
estável do Debian.
</p>

<p>
Um total de nove arquiteturas são oficialmente suportadas no <q>bookworm</q>:
<ul>
<li>PC 32 bits (i386) e PC 64 bits (amd64),</li>
<li>ARM 64 bits (arm64),</li>
<li>ARM EABI (armel),</li>
<li>ARMv7 (EABI hard-float ABI, armhf),</li>
<li>MIPS little-endian (mipsel),</li>
<li>MIPS 64 bits little-endian (mips64el),</li>
<li>PowerPC 64 bits little-endian (ppc64el),</li>
<li>IBM System z (s390x)</li> 
</ul>

O PC de 32 bits (i386) não suporta mais nenhum processador i586; o novo processador
mínimo requisitado é o i686. <i>Se sua máquina não for compatível com este
requisito, é recomendável que você fique com o Bullseye pelo restante de seu ciclo
de suporte.</i>
</p>

<p>A equipe Debian cloud publica o <q>bookworm</q> para vários serviços de
computação em nuvem:
<ul>
<li>Amazon EC2 (amd64 e arm64),</li>
<li>Microsoft Azure (amd64),</li>
<li>OpenStack (generic) (amd64, arm64, ppc64el),</li>
<li>GenericCloud (arm64, amd64),</li>
<li>NoCloud (amg64, arm64, ppc64el)</li>
</ul>
A imagem genericcloud deve ser compatível com qualquer ambiente virtualizado,
e também há uma imagem nocloud que é útil para testar o processo de construção.
</p>

<p>Os pacotes GRUB, por padrão, <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.pt.html#grub-os-prober">
não executarão mais o os-prober para detectar outros sistemas operacionais.</a></p>

<p>Entre os lançamentos, o comitê técnico resolveu que o Debian <q>bookworm</q>
deve <a href="https://wiki.debian.org/UsrMerge">suportar apenas o layout do sistema de arquivos merged-usr</a>, eliminando o suporte
para o layout non-merged-usr. Para sistemas instalados como buster ou bullseye
não haverá mudanças no sistema de arquivos; no entanto, os sistemas que usam a versão mais antiga
do layout serão convertidos durante a atualização.</p>


<h3>Quer experimentar?</h3>
<p>
Se você simplesmente deseja experimentar o Debian 12 <q>bookworm</q> sem
instalá-lo, você pode usar uma das <a href="$(HOME)/CD/live/">imagens live</a> disponíveis, que
carregam e executam o sistema operacional completo em um estado de somente
leitura através da memória do seu computador.
</p>

<p>
Essas imagens live são fornecidas para as arquiteturas <code>amd64</code> e
<code>i386</code> e estão disponíveis para DVDs, pendrives USB
e configurações de inicialização em rede. O(A) usuário(a) pode escolher dentre
diferentes ambientes de área de trabalho para experimentar: GNOME, KDE Plasma,
LXDE, LXQt, MATE e Xfce. O Debian <q>bookworm</q> Live tem uma imagem live padrão, então também é
possível experimentar um sistema Debian básico sem nenhuma das interfaces gráficas.
</p>

<p>
Se gostar do sistema operacional, você tem a opção de instalar
da imagem live para o disco rígido do seu computador. A imagem live
inclui o instalador independente com o nome Calamares bem como o instalador Debian padrão.
Mais informações estão disponíveis nas
<a href="$(HOME)/releases/bookworm/releasenotes">notas de lançamento</a> e nas
seções de <a href="$(HOME)/CD/live/">imagens de instalação live</a> do
site web do Debian.
</p>

<p>
Para instalar o Debian 12 <q>bookworm</q> diretamente no dispositivo de armazenamento
do seu computador, você pode escolher dentre uma variedade de tipos de mídia de instalação para <a href="https://www.debian.org/download">baixar</a>,
tais como: disco Blu-ray, DVD, CD, pendrive USB ou através de uma conexão de rede.

Veja o <a href="$(HOME)/releases/bookworm/installmanual">guia de instalação</a> para mais detalhes.
</p>

# Translators: some text taken from: 

<p>
O Debian agora pode ser instalado em 78 idiomas, com a maioria deles disponíveis
nas interfaces de usuário(a) baseadas tanto em texto quanto gráficas.
</p>

<p>
As imagens de instalação podem ser baixadas agora mesmo via
<a href="$(HOME)/CD/torrent-cd/">bittorrent</a> (o método recomendado),
<a href="$(HOME)/CD/jigdo-cd/#which">jigdo</a>, ou
<a href="$(HOME)/CD/http-ftp/">HTTP</a>; veja
<a href="$(HOME)/CD/">Debian em CDs</a> para mais informações. O <q>bookworm</q> logo
estará disponível também em DVD físico, CD-ROM e discos Blu-ray de vários <a href="$(HOME)/CD/vendors">vendedores</a>.
</p>


<h3>Atualizando o Debian</h3>
<p>
Atualizações para o Debian 12 <q>bookworm</q> da versão anterior, Debian 11
<q>bullseye</q>, são tratadas automaticamente pela ferramenta de gerenciamento
de pacotes APT para a maioria das configurações.
</p>

<p>Antes de atualizar seu sistema, é altamente recomendável fazer um backup
completo ou, pelo menos, fazer backup de quaisquer dados ou informações de
configuração que você não possa perder. As ferramentas e o processo de
atualização são bastante confiáveis, mas uma falha de hardware no meio de uma
atualização pode resultar em danos graves ao sistema.

As principais coisas que você desejará fazer backup são o conteúdo de /etc, 
/var/lib/dpkg, /var/lib/apt/extended_states e a saída de:

<code>$ dpkg --get-selections '*' # (as aspas simples são importantes)</code>

<p>Agradecemos qualquer informação de usuários(as) relacionada à atualização do <q>bullseye</q> 
para o <q>bookworm</q>. Por favor, compartilhe informações registrando um bug no 
<a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-about.pt.html#upgrade-reports">sistema
de acompanhamento de bugs do Debian</a> usando o pacote <b>upgrade-reports</b> com seus resultados.
</p>

<p>
Houve muito trabalho de desenvolvimento no instalador Debian, resultando em suporte de
hardware aprimorado e outros recursos, como correções para suporte gráfico em
UTM, correções para o carregador de fonte do GRUB, remoção da longa espera ao
final do processo de instalação e correções para a detecção de sistemas
inicializáveis por BIOS. Esta versão do instalador do Debian pode habilitar
firmware não livre quando necessário.</p>



<p>
O pacote <b>ntp</b> foi substituído pelo pacote <b>ntpsec</b>,
com o serviço de relógio do sistema padrão agora sendo <b>systemd-timesyncd</b>;
também há suporte para <b>chrony</b> e <b>openntpd</b>.
</p>


<p>Como firmwares <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.pt.html#non-free-split"><b>não livres</b> 
foram movidos para seu próprio local no repositório</a>, se você tiver
firmwares não livres instalados, é recomendável adicionar <b>non-free-firmware</b>
em seu APT source-list.</p>

<p>É aconselhável remover as entradas bullseye-backports do arquivo APT source-list
antes da atualização; após a atualização, considere adicionar <b>bookworm-backports</b>.

<p>
Para o <q>bookworm</q>, o repositório de segurança é nomeado <b>bookworm-security</b>; os(as)
usuários(as) devem adaptar seu APT source-list de acordo ao atualizar.

Se a sua configuração APT também envolve pinning ou <code>APT::Default-Release</code>,
é provável que sejam necessários ajustes para permitir a atualização dos pacotes para a nova versão estável.
Por favor, considere <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.pt.html#disable-apt-pinning">desabilitar APT pinning</a>.
</p>

<p>A atualização para OpenLDAP 2.5 inclui algumas <a href="$(HOME)/releases/testing/amd64/release-notes/ch-information.pt.html#openldap-2.5">alterações incompatíveis
que podem exigir intervenção manual</a>. Dependendo da configuração, o serviço
<b>slapd</b> pode permanecer parado após a atualização até que
novas alterações de configuração sejam concluídas.</p>


<p>O novo pacote <b>systemd-resolved</b> não será instalado automaticamente
em atualizações, pois <a href="$(HOME)/releases/testing/amd64/release-notes/ch-information.pt.html#systemd-resolved">foi dividido em
um pacote separado</a>. Se estiver usando o serviço de sistema systemd-resolved, por favor instale o novo pacote manualmente após
a atualização, e observe que, até que seja instalado, a resolução de DNS pode não
funcionar mais, pois o serviço não estará presente no sistema.</p>



<p>Há algumas <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.pt.html#changes-to-system-logging">mudanças no log do sistema</a>;
o pacote <b>rsyslog</b> não é mais necessário na maioria dos sistemas e não é instalado
por padrão. Os(As) usuários(as) podem mudar para <b>journalctl</b> ou usar a nova
<q>marca temporal (timestamps) de alta precisão</q> que o <b>rsyslog</b> agora usa.
</p>


<p><a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-upgrading.pt.html#trouble">Possíveis problemas durante a atualização</a> incluem
Conflicts (conflitos) ou loops Pre-Depends (loops de pré-dependências) que podem
ser resolvidos removendo e eliminando alguns pacotes ou forçando a reinstalação
de outros pacotes.
Preocupações adicionais são erros como <q>não foi possível executar a configuração
imediata...</q> para os quais será necessário manter <b>ambas</b> entradas,
<q>bullseye</q> (que acabou de ser removido) e <q>bookworm</q>
(que acabou de ser adicionado), no arquivo APT source-list, e conflitos de
arquivos que podem exigir a remoção forçada de pacotes.
Como mencionado, o backup do sistema é a chave para uma atualização tranquila,
caso ocorra algum erro indesejado.</p>


<p>Existem alguns pacotes para os quais o Debian não pode prometer o fornecimento de backports mínimos
para problemas de segurança. Por favor, veja as <a href="$(HOME)/releases/bookworm/amd64/release-notes/ch-information.pt.html#limited-security-support">limitações no suporte de segurança</a>.</p>


<p>
Como sempre, os sistemas Debian podem ser atualizados sem problemas, no local,
sem qualquer tempo de inatividade forçado, mas é altamente recomendável ler as
<a href="$(HOME)/releases/bookworm/releasenotes">notas de lançamento</a>
assim como o <a href="$(HOME)/releases/bookworm/installmanual">guia de
instalação</a> para identificar possíveis problemas e para obter instruções detalhadas
sobre instalação e atualização. As notas de lançamento serão melhoradas e
traduzidas para idiomas adicionais nas semanas posteriores ao lançamento.
</p>


<h2>Sobre o Debian</h2>

<p>
O Debian é um sistema operacional livre, desenvolvido por
milhares de voluntários(as) de todo o mundo que colaboram através da
Internet. Os pontos fortes do projeto Debian são sua base de voluntários(as),
sua dedicação ao Contrato Social do Debian e ao Software Livre, e seu
compromisso em fornecer o melhor sistema operacional possível. Este novo
lançamento é mais um passo importante nessa direção.
</p>


<h2>Informações de contato</h2>

<p>
Para mais informações, por favor visite as páginas web do Debian em
<a href="$(HOME)/">https://www.debian.org/</a> ou envie um e-mail (em inglês) para
&lt;press@debian.org&gt;.
</p>
